TatraPay is operated by Tatra Banka, a. s., Slovakia
Payment gateway for Ubercart
www.tatrabanka.sk	

Developed by Matus Lestan matus.lestan@gmail.com
Inspired by Drupal 6 tatrapay module by Marek Zapach & Peter Pokrivcak

Installation:
No special instructions to install. Upload and enable module as normal.

Configure:
In admin/store/settings/payment enable TatraPay as a payment method. 
Click edit Tatrapay method (admin/store/settings/payment/method/tatrapay)
Enter the TatraPay MID and Checksome key you were given by TatraPay
Enter test TatraPay MID and Checksome key if you have one (optional)
Select transaction mode to be Test or Production (defaults to Test). Use test to check your installation. Testing instructions below.
Change return URL to suit your needs. The default is enough to show transaction messages and new user register, etc.
Change titles and instructions to suit your layout.